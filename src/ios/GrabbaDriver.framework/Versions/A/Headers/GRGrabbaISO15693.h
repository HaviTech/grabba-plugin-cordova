//
//  GRGrabbaISO15693.h
//  GrabbaDriver
//
//  Created by Grabba Developer on 4/10/12.
//  Copyright (c) 2012 Grabba International. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FIND_VICC_RETRY     5
#define READ_BLOCK_RETRY    10

/**
 * Provides access to ISO15693 capabilities of an attached Grabba device.
 */
@interface GRGrabbaISO15693 : NSObject

/**
 * Get an instance of this class to invoke functions.
 *
 * @return an instance of this class through which ISO15693 functionality can be used.
 */
+ (GRGrabbaISO15693*) sharedInstance;

/**
 * Determines if the currently connected Grabba supports ISO15693 functionality.
 *
 * @return YES if the Grabba supports ISO15693 functions, NO otherwise.
 */
- (BOOL) isISO15693Supported;

/**
 * Look for the Vicinity Integrated Circuit Card (VICC)
 *
 * @param error return error message if there is any.  Nil otherwise.
 * @return Card Unique identifier (UID)
 */
-(NSData*) findVICC: (NSError**) error;

/**
 * Select ISO15693 card.
 *
 * @param UID Card unique ID.
 * @param error return error message if there is any.  Nil otherwise.
 * @return Card response.
 */
-(NSData*) selectVICC: (NSData*) UID : (NSError**) error;

/**
 * Read a single block from the card.
 *
 * @param blockNumber Block index of the card
 * @param error return error message if there is any.  Nil otherwise.
 * @return Block data read.
 */
-(NSData*) readBlock: (uint8_t) blockNumber : (NSError**) error;

/**
 * Write a single block into the card.
 *
 * @param blockNumber Block index of the card
 * @param data Block data
 * @param error return error message if there is any.  Nil otherwise.
 */
-(void) writeBlock: (uint8_t) blockNumber : (NSData*) data : (NSError**) error;

/**
 * Read multiple data blocks of an ISO15693 tag on the field.
 *
 * @param startBlockNumber
 *          The starting block number to read.
 * @param blockCount
 *          Number of blocks to read from starting block.
 *          A value of 0 will result in a single block read.
 *          Maximum number of blocks to read in a single transaction is 60 (240 Bytes).
 * @param error
 *          Return error message if there is any.  Nil otherwise.
 * @return if successful the data in the block's will be returned, otherwise a empty NSData  is returned.
 */
-(NSData*) readMultiBlock: (uint8_t) startBlockNumber : (uint8_t) blockCount : (NSError**) error;

/**
 * Write to a range of memory blocks of an ISO15693 tag on the field.
 *
 * @param startBlockNumber
 *          The starting block number to write.
 * @param blockCount
 *          The number of blocks to be written after the first block.
 *          A value of 0 will result in a single block write.
 *          Maximum number of blocks to write to in a single transaction is 60 (240 Bytes).
 * @param data
 *          Block data to write to card.
 *          Attempting to write the wrong data size to a block will result in an IO error.
 * @param error return error message if there is any.  Nil otherwise.
 */
-(void) writeMultiBlock: (uint8_t) startBlockNumber : (uint8_t) blockCount : (NSData*) data :  (NSError**) error;

/**
 * This function let users to communiate with the card with specific flag, command and parameters.  For example, Set EAS, Write AFI.  Please read the datasheets for ISO15693 carefully before you apply this procedure into your code.
 *
 * @param flag Flag
 * @param command Command code
 * @param parameter Parameters
 * @param error return error message if there is any.  Nil otherwise.
 * @return Raw response from the card.
 */
-(NSData*) exchangeCommand: (uint8_t) flag : (uint8_t) command : (NSData*) parameter : (NSError**) error;

/**
 * Select a Pico tag and return the tag's ID
 * @param error pointer to NSError*. Will be set to non-nil on error
 * @return card ID
 */
- (NSData*) selectPicoTagWithError:(NSError**)error;

/**
 * Read the given block from the previously selected Pico tag.
 * @param block the block number to read from
 * @param error pointer to NSError*. Will be set to non-nil on error
 * @return block contents
 */
- (NSData*) readPicoTagBlock:(NSInteger)block withError:(NSError**)error;

/**
 * Iteratively reads blocks from the previously selected Pico tag.
 * This function will continuously call the the  PicoTag read function reading the specified numberOfBlocks starting from the startingBlock.
 * Note this function can take a significant time (approximately 40ms/block) when a large numberOfBlocks is specified.
 * Should an error occur during communication, only the successful blocks up to the error will be returned. In the event that no blocks are read empty NSData is returned.
 * @param startBlock
 *      The block number to start reading from
 * @param numberOfBlocks
 *      The number of blocks to read from the startingBlock.
 *      Providing a value of 0 will result in empty data being returned.
 * @param err
 *      Pointer to NSError*. Will be set to non-nil on error
 * @return block contents
 */
- (NSData*) readPicoTagMultiBlock: (NSInteger) startBlock : (NSInteger) numberOfBlocks : (NSError**)err;

/**
 * Iteratively writes blocks from the previously selected Pico tag.
 * This function will continuously call the the  PicoTag write function and write the specified numberOfBlocks starting from the startingBlock.
 * Note this function can take a significant time (approximately 40ms/block) when a large numberOfBlocks is specified.
 * Should an error occur during communication, only the successful blocks up to the error will be written, and the function will return the number of succesful blocks.
 * @param startBlock
 *      The block number to start reading from
 * @param numberOfBlocks
 *      The number of blocks to write to, from the startingBlock.
 *      Providing a value of 0 will result in empty data being returned.
 * @param blockData
 *      The block data to write to the tag. The length in bytes must be exactly 8*numberOfBlocks.
 * @param err
 *      Pointer to NSError*. Will be set to non-nil on error
 * @return number of succesful block writes.
 */
- (NSInteger) writePicoTagMultiBlock: (NSInteger) startBlock : (NSInteger) numberOfBlocks : (NSData*) blockData : (NSError**)err;

/**
 * Write the given block to the previously selected Pico tag
 * @param block the block number to write to
 * @param data the data to write - MUST BE 8 BYTES
 * @param error pointer to NSError*. Will be set to non-nil on error
 */
- (void) writePicoTagBlock:(NSInteger)block withData:(NSData*)data andError:(NSError**)error;


/**
 * Powers down the RF field.<br>
 *
 * Turns off the RF field. PICCs on the field will not hold their state once the RF field is turned off.
 */
+ (BOOL) powerdown:(NSError**)error;

@end
