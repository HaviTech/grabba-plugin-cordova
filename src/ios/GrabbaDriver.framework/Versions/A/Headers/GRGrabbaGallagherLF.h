//
//  GRGrabbaGallagherLF.h
//  GrabbaDriver
//
//  Created by Wayne Uroda on 2/02/2016.
//  Copyright © 2016 Grabba International. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef GRGrabbaGallagherLF_h
#define GRGrabbaGallagherLF_h


#endif /* GRGrabbaGallagherLF_h */

/**
 * Provides access to GallagherLF capabilities of an attached "-GA" Grabba device.
 *
 * -GA is a Gallagher dual frequency reader, containing high and low frequency RFID circuitry.
 * This class is intended to provided access to the low frequency component of the reader.
 * *NOTE*: This is only available on Gallagher Dual Frequency devices (Model number with suffix -GA). Calling findGallagherPICC function on Non-Gallagher devices will result in a functionNotSupported error, and empty data being returned.
 *
 * The following functions are supported:<br>
 *
 * - isGallagherLfSupported (Returns true if GallagherLF functionality is supported)
 * - findGallagherPICC (Returns the UID of a GallagherLF tag found).
 */
@interface GRGrabbaGallagherLF : NSObject

/**
 *  Singleton class -> direct init is disabled. Call sharedInstance instead.
 */
- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)sharedInstance;

/**
 * Determines if the currently connected Grabba supports GallagherLF functionality.
 *
 * @return true if the Grabba supports GallagherLF functions, false otherwise.
 */
- (BOOL) isGallagherLFSupported;

/**
 * Returns the UID and GallagherLF tag type if data is found.<br>
 * Calling this function will power down any other RFID fields, and disconnect any current card communications.
 *
 * Gallagher LF module will also power down automatically at completion of this function.
 *
 * @return The UID of the GallagherLF tag that is found on the field. If there is more than one the first resolved UID will be returned. empty data if error.
 *
 * If called on a device which does not support this function, a functionNotSupported error will be given and empty data returned.
 */
- (NSData *) findGallagherPICC:(NSError **)error;

@end
