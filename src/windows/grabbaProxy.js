// cordova wraps this file inside something which looks like: "cordova.define("com.grabba.cordova.plugin.GrabbaProxy", function(require, exports, module) { ......... });"

    // Listener objects - for receiving asynchronous event callbacks from the driver
    var barcodeListener = new GrabbaDriver.Barcode.Listener();
    var buttonListener = new GrabbaDriver.ButtonListener();
    var connectionListener = new GrabbaDriver.ConnectionListener();
    var fingerprintListener = new GrabbaDriver.Fingerprint.Listener();
    var magstripeListener = new GrabbaDriver.Magstripe.Listener();
    var MRTD_Listener = new GrabbaDriver.MRTD.Listener();
    var MRZ_Listener = new GrabbaDriver.MRZ.Listener();
    var proxcardListener = new GrabbaDriver.Proxcard.Listener();

    //Convenience function to set/release callbacks
    var setCallback = function (newCallback, oldCallback) {
        if (oldCallback) {
            oldCallback(null, {
                status: cordova.callbackStatus.NO_RESULT,
                keepCallback: false
            });
        }

        return newCallback;
    };
    //Convenience object to keep callbacks
    var keepCallback = {
        status: cordova.callbackStatus.OK,
        keepCallback: true
    };

    //Convenience object to discard callbacks
    var discardCallback = {
        status: cordova.callbackStatus.OK,
        keepCallback: false
    };

    // Grabba callbacks
    // WinJS syntax for event handlers is that a single 'event' parameter contains all of the delegate params - first one in 'target' field, others in 'detail' array field

    var GrabbaConnectedCallback = null;
    var GrabbaConnected = function (event) {
        if (GrabbaConnectedCallback) {
            GrabbaConnectedCallback(true, keepCallback);
        }
    };
    var GrabbaDisconnectedCallback = null;
    var GrabbaDisconnected = function (event) {
        if (GrabbaDisconnectedCallback) {
            GrabbaDisconnectedCallback(false, keepCallback);
        }
    };

    var GrabbaLeftButtonCallback = null;
    var GrabbaLeftButton = function (event) {
        if (GrabbaLeftButtonCallback) {
            GrabbaLeftButtonCallback(event.target, keepCallback);
        }
    };

    var GrabbaRightButtonCallback = null;
    var GrabbaRightButton = function (event) {
        if (GrabbaRightButtonCallback) {
            GrabbaRightButtonCallback(event.target, keepCallback);
        }
    };

    //GrabbaBarcode callbacks
    var GrabbaBarcodeTriggeredCallback = null;
    var GrabbaBarcodeTriggered = function (event) {
        if (GrabbaBarcodeTriggeredCallback) {
            GrabbaBarcodeTriggeredCallback(null, keepCallback);
        }
    };
    var GrabbaBarcodeScannedCallback = null;
    var GrabbaBarcodeScanned = function (event) {
        if (GrabbaBarcodeScannedCallback) {
            GrabbaBarcodeScannedCallback({
                data: event.target.barcode,
                symbology: event.target.symbologyName(),
                symbologyInt: event.target.symbology
            }, keepCallback);
        }
    };
    var GrabbaBarcodeStoppedCallback = null;
    var GrabbaBarcodeStopped = function (event) {
        if (GrabbaBarcodeStoppedCallback) {
            GrabbaBarcodeStoppedCallback(null, keepCallback);
        }
    };
    var GrabbaBarcodeTimedOutCallback = null;
    var GrabbaBarcodeTimedOut = function (event) {
        if (GrabbaBarcodeTimedOutCallback) {
            GrabbaBarcodeTimedOutCallback(null, keepCallback);
        }
    };

    //GrabbaProxcard callbacks
    var GrabbaProxcardTriggeredCallback = null;
    var GrabbaProxcardTriggered = function (event) {
        if (GrabbaProxcardTriggeredCallback) {
            GrabbaProxcardTriggeredCallback(null, keepCallback);
        }
    };
    var GrabbaProxcardScannedCallback = null;
    var GrabbaProxcardScanned = function (event) {
        if (GrabbaProxcardScannedCallback) {
            GrabbaProxcardScannedCallback({
                data: event.target.contents,
                type: GrabbaDriver.Proxcard.Data.typeString(event.target.type),
                typeInt: event.target.type
            }, keepCallback);
        }
    };
    var GrabbaProxcardStoppedCallback = null;
    var GrabbaProxcardStopped = function (event) {
        if (GrabbaProxcardStoppedCallback) {
            GrabbaProxcardStoppedCallback(null, keepCallback);
        }
    };
    var GrabbaProxcardTimedOutCallback = null;
    var GrabbaProxcardTimedOut = function (event) {
        if (GrabbaProxcardTimedOutCallback) {
            GrabbaProxcardTimedOutCallback(null, keepCallback);
        }
    };

    //GrabbaPassport callbacks
    var GrabbaPassportReadCallback = null;
    var GrabbaPassportRead = function (event) {
        if (GrabbaPassportReadCallback) {
            GrabbaPassportReadCallback(event.target.text, keepCallback);
        }
    };


    //GrabbaMagstripe callbacks
    var GrabbaMagstripeReadCallback = null;
    var GrabbaMagstripeRead = function (event) {
        if (GrabbaMagstripeReadCallback) {
            GrabbaMagstripeReadCallback({
                track1: event.target.track1,
                track2: event.target.track2,
                track3: event.target.track3
            }, keepCallback);
        }
    };

    //GrabbaFingerprint callbacks
    var GrabbaFingerprintTemplateCallback = null;
    var GrabbaFingerprintTemplate = function (event) {
        if (GrabbaFingerprintTemplateCallback) {
            GrabbaFingerprintTemplateCallback({
                data: event.target.data,
                type: event.target.typeName(),
                typeInt: event.target.templateType
            }, keepCallback);
        }
    };
    var GrabbaFingerprintImageCallback = null;
    var GrabbaFingerprintImage = function (event) {
        if (GrabbaFingerprintImageCallback) {
            GrabbaFingerprintImageCallback({
                data: event.target.data,
                numRows: event.target.rows,
                numColumns: event.target.columns,
                type: event.target.typeName(),
                typeInt: event.target.imageType
            }, keepCallback);
        }
    };
    var GrabbaFingerprintUserMessageCallback = null;
    var GrabbaFingerprintUserMessage = function (event) {
        if (GrabbaFingerprintUserMessageCallback) {
            GrabbaFingerprintUserMessageCallback({
                text: event.target.message(),
                id: event.target.id,
                number: event.target.count,
                total: event.target.total,
                userRecord: event.target.userRecord
            }, keepCallback);
        }
    };

    //GrabbaMRTD callbacks
    var GrabbaMRTDProgressCallback = null;
    var GrabbaMRTDProgress = function (event) {
        if (GrabbaMRTDProgressCallback) {
            GrabbaMRTDProgressCallback(event.target, keepCallback);
        }
    };
    var GrabbaMRTDCompleteCallback = null;
    var GrabbaMRTDComplete = function (event) {
        if (GrabbaMRTDCompleteCallback) {
            GrabbaMRTDCompleteCallback(event.target.get(), discardCallback); // for now, this callback is a one-shot; likely to change in future update to Cordova driver
        }
    };

    module.exports = {
        Grabba: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'isConnected') {
                callback(GrabbaDriver.CoreAPI.connected());
            }
            else if (command.command == 'clearExclusiveAccessCallback') {
                //There is no concept of exclusive access on Windows.
            }
            else if (command.command == 'registerExclusiveAccessCallback') {
                //There is no concept of exclusive access on Windows
                //We will invoke an exclusive access granted callback immediately.
                callback(true);
            }
            else if (command.command == 'clearConnectionCallback') {
                connectionListener.onconnectevent = setCallback(null, GrabbaConnectedCallback);
                connectionListener.ondisconnectevent = setCallback(null, GrabbaDisconnectedCallback);
            }
            else if (command.command == 'registerConnectionCallback') {
                GrabbaConnectedCallback = setCallback(callback, GrabbaConnectedCallback);
                connectionListener.onconnectevent = GrabbaConnected;
                GrabbaDisconnectedCallback = setCallback(callback, GrabbaDisconnectedCallback);
                connectionListener.ondisconnectevent = GrabbaDisconnected;
            }
            else if (command.command == 'clearButtonCallbacks') {
                buttonListener.onleftbuttonevent = setCallback(null, GrabbaLeftButtonCallback);
                buttonListener.onrightbuttonevent = setCallback(null, GrabbaRightButtonCallback);
            }
            else if (command.command == 'registerLeftButtonCallback') {
                GrabbaLeftButtonCallback = setCallback(callback, GrabbaLeftButtonCallback);
                buttonListener.onleftbuttonevent = GrabbaLeftButton;
            }
            else if (command.command == 'registerRightButtonCallback') {
                GrabbaRightButtonCallback = setCallback(callback, GrabbaRightButtonCallback);
                buttonListener.onrightbuttonevent = GrabbaRightButton;
            }
            else if (command.command == "getDriverVersion") {
                callback(GrabbaDriver.CoreAPI.driverVersion());
            }
            else if (command.command == 'getBatteryLevel') {
                callback(GrabbaDriver.CoreAPI.batteryLevel());
            }
            else if (command.command == 'getFirmwareVersion') {
                callback(GrabbaDriver.FirmwareAPI.version());
            }
            else if (command.command == 'getHardwareVersion') {
                callback(GrabbaDriver.CoreAPI.hardwareVersion());
            }
            else if (command.command == 'getModel') {
                callback(GrabbaDriver.CoreAPI.deviceModel());
            }
            else if (command.command == 'getLeftButtonState') {
                callback(GrabbaDriver.ButtonAPI.leftButtonState());
            }
            else if (command.command == 'getRightButtonState') {
                callback(GrabbaDriver.ButtonAPI.rightButtonState());
            }
            else if (command.command == 'getSerialNumber') {
                callback(GrabbaDriver.CoreAPI.serialNumber());
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaBarcode: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'trigger') {
                if (command.enable) {
                    var error = new GrabbaDriver.ErrorCode();
                    GrabbaDriver.Barcode.API.startScan(error);
                    if (error.error) {
                        onError(error.details());
                    }
                }
                else {
                    var error = new GrabbaDriver.ErrorCode();
                    GrabbaDriver.Barcode.API.stopScan(false, error);
                    if (error.error) {
                        onError(error.details());
                    }
                }
            }
            else if (command.command == 'isSupported') {
                callback(GrabbaDriver.Barcode.API.supported());
            }
            else if (command.command == 'clearCallbacks') {
                barcodeListener.onstartevent = setCallback(null, GrabbaBarcodeTriggeredCallback);
                barcodeListener.ontimeoutevent = setCallback(null, GrabbaBarcodeTimedOutCallback);
                barcodeListener.onstopevent = setCallback(null, GrabbaBarcodeStoppedCallback);
                barcodeListener.ondataevent = setCallback(null, GrabbaBarcodeScannedCallback);
            }
            else if (command.command == 'registerCallbackTriggered') {
                GrabbaBarcodeTriggeredCallback = setCallback(callback, GrabbaBarcodeTriggeredCallback);
                barcodeListener.onstartevent = GrabbaBarcodeTriggered;
            }
            else if (command.command == 'registerCallbackTimeout') {
                GrabbaBarcodeTimedOutCallback = setCallback(callback, GrabbaBarcodeTimedOutCallback);
                barcodeListener.ontimeoutevent = GrabbaBarcodeTimedOut;
            }
            else if (command.command == 'registerCallbackStopped') {
                GrabbaBarcodeStoppedCallback = setCallback(callback, GrabbaBarcodeStoppedCallback);
                barcodeListener.onstopevent = GrabbaBarcodeStopped;
            }
            else if (command.command == 'registerCallbackScanned') {
                GrabbaBarcodeScannedCallback = setCallback(callback, GrabbaBarcodeScannedCallback);
                barcodeListener.ondataevent = GrabbaBarcodeScanned;
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaProxcard: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'trigger') {
                if (command.enable) {
                    var error = new GrabbaDriver.ErrorCode();
                    GrabbaDriver.Proxcard.API.startScan(error);
                    if (error.error) {
                        onError(error.details());
                    }
                }
                else {
                    var error = new GrabbaDriver.ErrorCode();
                    GrabbaDriver.Proxcard.API.stopScan(false, error);
                    if (error.error) {
                        onError(error.details());
                    }
                }
            }
            else if (command.command == 'isSupported') {
                callback(GrabbaDriver.Proxcard.API.supported());
            }
            else if (command.command == 'clearCallbacks') {
                proxcardListener.onstartevent = setCallback(null, GrabbaProxcardTriggeredCallback);
                proxcardListener.ontimeoutevent = setCallback(null, GrabbaProxcardTimedOutCallback);
                proxcardListener.onstopevent = setCallback(null, GrabbaProxcardStoppedCallback);
                proxcardListener.ondataevent = setCallback(null, GrabbaProxcardScannedCallback);
            }
            else if (command.command == 'registerCallbackTriggered') {
                GrabbaProxcardTriggeredCallback = setCallback(callback, GrabbaProxcardTriggeredCallback);
                proxcardListener.onstartevent = GrabbaProxcardTriggered;
            }
            else if (command.command == 'registerCallbackTimeout') {
                GrabbaProxcardTimedOutCallback = setCallback(callback, GrabbaProxcardTimedOutCallback);
                proxcardListener.ontimeoutevent = GrabbaProxcardTimedOut;
            }
            else if (command.command == 'registerCallbackStopped') {
                GrabbaProxcardStoppedCallback = setCallback(callback, GrabbaProxcardStoppedCallback);
                proxcardListener.onstopevent = GrabbaProxcardStopped;
            }
            else if (command.command == 'registerCallbackScanned') {
                GrabbaProxcardScannedCallback = setCallback(callback, GrabbaProxcardScannedCallback);
                proxcardListener.ondataevent = GrabbaProxcardScanned;
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaPassport: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'isSupported') {
                callback(GrabbaDriver.MRZ.API.supported());
            }
            else if (command.command == 'clearCallbacks') {
                MRZ_Listener.ondataevent = setCallback(null, GrabbaPassportReadCallback);
            }
            else if (command.command == 'registerCallback') {
                GrabbaPassportReadCallback = setCallback(callback, GrabbaPassportReadCallback);
                MRZ_Listener.ondataevent = GrabbaPassportRead;
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaMagstripe: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'isSupported') {
                callback(GrabbaDriver.Magstripe.API.supported());
            }
            else if (command.command == 'clearCallbacks') {
                magstripeListener.ondataevent = setCallback(null, GrabbaMagstripeReadCallback);
            }
            else if (command.command == 'registerReadCallback') {
                GrabbaMagstripeReadCallback = setCallback(callback, GrabbaMagstripeReadCallback);
                magstripeListener.ondataevent = GrabbaMagstripeRead;
            }
            else if (command.command == 'registerRawReadCallback') {
                onError('Magstripe raw read is not currently supported on this platform.');
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaFingerprint: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'isSupported') {
                callback(GrabbaDriver.Fingerprint.API.supported);
            }
            else if (command.command == 'enrolFingerprint') {
                var error = new GrabbaDriver.ErrorCode();
                // Note: number-of-fingers parameters has been removed from the Windows API, and will be removed from others in future
                GrabbaDriver.Fingerprint.API.capture(command.templateType, command.imageType, command.numAcquisitions > 1, error);
                if (error.error) {
                    onError(error.details());
                }
            }
            else if (command.command == 'enrolFingerprintToDatabase') {
                onError('Fingerprint database functionality is not currently supported on this platform.')
            }
            else if (command.command == 'identifyFingerprint') {
                onError('Fingerprint database functionality is not currently supported on this platform.')
            }
            else if (command.command == 'verifyFingerprint') {
                var error = new GrabbaDriver.ErrorCode();
                GrabbaDriver.Fingerprint.API.verify(command.data, command.typeInt, error);
                if (error.error) {
                    onError(error.details());
                }
            }
            else if (command.command == 'clearFingerprintDatabase') {
                onError('Fingerprint database functionality is not currently supported on this platform.')
            }
            else if (command.command == 'resetFingerprintDatabase') {
                onError('Fingerprint database functionality is not currently supported on this platform.')
            }
            else if (command.command == 'compareTemplates') {
                onError('Compare templates functionality is not currently supported on this platform.')
            }
            else if (command.command == 'abort') {
                GrabbaDriver.Fingerprint.API.abort(true);
            }
            else if (command.command == 'convertImageToBase64') {
                var image = new GrabbaDriver.Fingerprint.Image(command.image.data, command.image.typeInt, command.image.numRows, command.image.numColumns);
                var error = new GrabbaDriver.ErrorCode();
                callback(image.generateBase64(error));
                if (error.error)
                {
                    onError(error.details());
                }
            }
            else if (command.command == 'registerTemplateDataCallback') {
                GrabbaFingerprintTemplateCallback = setCallback(callback, GrabbaFingerprintTemplateCallback);
                fingerprintListener.ontemplateevent = GrabbaFingerprintTemplate;
            }
            else if (command.command == 'registerImageDataEventCallback') {
                GrabbaFingerprintImageCallback = setCallback(callback, GrabbaFingerprintImageCallback);
                fingerprintListener.onimageevent = GrabbaFingerprintImage;
            }
            else if (command.command == 'registerUserMessageEventCallback') {
                GrabbaFingerprintUserMessageCallback = setCallback(callback, GrabbaFingerprintUserMessageCallback);
                fingerprintListener.onusermessageevent = GrabbaFingerprintUserMessage;
            }
            else if (command.command == 'clearCallbacks') {
                fingerprintListener.onusermessageevent = setCallback(null, GrabbaFingerprintUserMessageCallback);
                fingerprintListener.ontemplateevent = setCallback(null, GrabbaFingerprintImageCallback);
                fingerprintListener.onimageevent = setCallback(null, GrabbaFingerprintTemplateCallback);
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaMRTD: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'isSupported') {
                callback(GrabbaDriver.MRTD.API.supported());
            }
            else if (command.command == "getDataFromMRZ") {
                // Set up the callback to trigger as one-shots from the MRTD listener object
                // Note: this function doesn't presently pick up the error event; this should be added in a future plugin release
                GrabbaMRTDCompleteCallback = setCallback(callback, GrabbaMRTDCompleteCallback);
                MRTD_Listener.ondataevent = GrabbaMRTDComplete;

                // Initiate the read operation
                var error = new GrabbaDriver.ErrorCode();
                GrabbaDriver.MRTD.API.startRead(command.trackData, command.fileID, error);
                if (error.error) {
                    onError(error.details());
                }
            }
            else if (command.command == "registerCallbackMRTDProgress") {
                GrabbaMRTDProgressCallback = setCallback(callback, GrabbaMRTDProgressCallback);
                MRTD_Listener.onprogressevent = GrabbaMRTDProgress;
            }
            else if (command.command == 'clearCallbacks') {
                MRTD_Listener.ondataevent = setCallback(null, GrabbaMRTDCompleteCallback);
                MRTD_Listener.onprogressevent = setCallback(null, GrabbaMRTDProgressCallback)
            }
            else {
                onError('Invalid action');
            }
        },
        GrabbaUtil: function (callback, onError, params) {
            var command = params[0];
            if (command.command == 'convertJpegToBase64') {
                var image = new GrabbaDriver.MRTD.Image(command.image, command.offset);
                callback(image.encodeAsBase64());
            }
            else {
                onError('Invalid action');
            }
        }
    };

    require("cordova/exec/proxy").add("GrabbaPlugin", module.exports);

    // Handle Cordova 'resume' life-cycle event by opening the Grabba driver
    function onResume()
    {
        GrabbaDriver.CoreAPI.open();
    }

    // Handle Cordova 'pause' life-cycle event by closing the Grabba driver
    function onPause() {
        GrabbaDriver.CoreAPI.close();
    }

    // Set up life-cycle event handlers, and manually trigger the resume handler to init the driver
    document.addEventListener("pause", onPause, false);
    document.addEventListener("resume", onResume, false);
    onResume();
